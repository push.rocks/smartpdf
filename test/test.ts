import { expect, tap } from '@pushrocks/tapbundle';
import * as smartpdf from '../ts/index.js';

let testSmartPdf: smartpdf.SmartPdf;

tap.test('should create a valid instance of smartpdf', async () => {
  testSmartPdf = new smartpdf.SmartPdf();
  expect(testSmartPdf).toBeInstanceOf(smartpdf.SmartPdf);
});

tap.test('should start the instance', async () => {
  await testSmartPdf.start();
});

tap.test('should create a pdf from html string', async () => {
  await testSmartPdf.getA4PdfResultForHtmlString('hi');
});

tap.test('should create a pdf from html string', async () => {
  await testSmartPdf.getA4PdfResultForHtmlString('hi');
});

tap.test('should create a pdf from website as A4', async () => {
  await testSmartPdf.getPdfResultForWebsite('https://www.wikipedia.org');
});

tap.test('should create a pdf from website as single page PDF', async () => {
  await testSmartPdf.getFullWebsiteAsSinglePdf('https://www.wikipedia.org');
});

tap.test('should create a valid PDFResult', async () => {
  const writePDfToDisk = async (urlArg: string, fileName: string) => {
    const pdfResult = await testSmartPdf.getFullWebsiteAsSinglePdf(urlArg);
    expect(pdfResult.buffer).toBeInstanceOf(Buffer);
    const fs = await import('fs');

    if (!fs.existsSync('.nogit/')) {
      fs.mkdirSync('.nogit/');
    }
    fs.writeFileSync(`.nogit/${fileName}`, pdfResult.buffer as Buffer);
  };
  await writePDfToDisk('https://maintainedby.lossless.com/', '1.pdf');
  await writePDfToDisk('https://rendertron.lossless.one/render/https://layer.io', '2.pdf');
});

tap.test('should combine pdfs', async () => {
  const fs = await import('fs');
  const pdf1 = await testSmartPdf.readFileToPdfObject('.nogit/1.pdf');
  const pdf2 = await testSmartPdf.readFileToPdfObject('.nogit/2.pdf');
  fs.writeFileSync(
    `.nogit/combined.pdf`,
    (await testSmartPdf.mergePdfs([pdf1, pdf2])).buffer as Buffer
  );
});

tap.test('should be able to close properly', async () => {
  await testSmartPdf.stop();
});

tap.start();
