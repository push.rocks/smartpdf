/*
 * pdf candidate is a construct used internally for mapping html
 * to pdf buffers delivered by puppeteer
 */
import * as plugins from './smartpdf.plugins.js';

export class PdfCandidate {
  public htmlString: string;
  public pdfId = plugins.smartunique.shortId();
  public doneDeferred = plugins.smartpromise.defer();

  constructor(htmlStringArg: string) {
    this.htmlString = htmlStringArg;
  }
}
