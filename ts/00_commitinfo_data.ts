/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartpdf',
  version: '3.0.15',
  description: 'create pdfs on the fly'
}
