// compatibility
declare global {
  interface Element {}
  interface Node {}
  interface NodeListOf<TNode = Node> {}
}

// normal
export * from './smartpdf.classes.smartpdf.js';

// additional types
import type * as tsclassTypes from '@tsclass/tsclass';
type IPdf = tsclassTypes.business.IPdf;

export type {
  IPdf
}